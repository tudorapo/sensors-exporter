FROM python:3-slim-bullseye

RUN apt-get update
RUN apt-get install -y git python3-pip lm-sensors
RUN adduser --disabled-login sensor
USER sensor
WORKDIR /home/sensor
RUN git clone https://gitlab.com/tudorapo/sensors-exporter.git
RUN python3 -m venv sensors-exporter/venv
RUN . sensors-exporter/venv/bin/activate
RUN pip install --upgrade pip
RUN pip install -r sensors-exporter/requirements.txt


CMD [ "python3", "sensors-exporter/sensors.py" ]
