import random
import time
import subprocess
import json
import argparse
import os
import sys
from prometheus_client import start_http_server, Summary, Gauge

parser = argparse.ArgumentParser()
parser.add_argument('-p', '--port', 
        help = 'Port to open the http server', 
        type = int, 
        default = 8000)
parser.add_argument('-t', '--cycle_time', 
        help = 'How often collect the data, in seconds',
        type = int,
        default = 15)
parser.add_argument('-c', '--chip', 
        help = 'Chips to display, space separated list',
        nargs = '*',
        default = '')
args = parser.parse_args()

if 'SENSORS_PORT' in os.environ:
    args.port = int(os.environ['SENSORS_PORT'])

if 'SENSORS_CYCLETIME' in os.environ:
    args.cycle_time = int(os.environ['SENSORS_CYCLETIME'])

if 'SENSORS_SENSORS' in os.environ:
    args.chip = os.environ['SENSORS_SENSORS'].split()

def collect(gauges):
    command = ['sensors', '-j'] 
    data = subprocess.run(command, 
            stdout = subprocess.PIPE, 
            stderr = subprocess.DEVNULL)
    chips = json.loads(data.stdout)
    for chip in chips:
        if args.chip and not chip in args.chip:
            continue
        for device in chips[chip]:
            if type(chips[chip][device]) is dict:
                for sensor in chips[chip][device]:
                    if sensor.endswith('_input'):
                        gauges.labels(chip, device, sensor).set(int(chips[chip][device][sensor]))

gauges = Gauge('sensors', 'lm-sensors data', ['chip', 'device', 'sensor'])

if __name__ == '__main__':
    # Start up the server to expose the metrics.
    print(f'Starting the sensors collector on the port {args.port},',
            f'cycle time {args.cycle_time}',
            file=sys.stderr)
    start_http_server(args.port)
    # Generate some requests.
    while True:
        collect(gauges)
        time.sleep(args.cycle_time)


